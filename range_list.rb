#encoding: utf-8

#require 'byebug'

class RangeList
  attr_accessor :ranges
  def initialize
    @ranges = []
  end

  def add(range)
    return if cover? range
    #byebug

    if exclude? range
      @ranges << (range[0]..range[1])
    elsif less_than_min_but_max_in? range
      if @ranges.first.max < range.max
        @ranges.shift
        @ranges.prepend(range[0]..@ranges.first.max)
      end
    elsif min_in_but_max_exclude? range
      element = @ranges.select do |r|
        r.cover? range.min
      end

      element = element.first
      index = @ranges.index(element)
      @ranges[index] = (element.min)..(range.max)
    end
  end

  def remove(range)
    return if exclude? range
    return if range.first == range.last
    range = [range.first,
      range.last > range.first ? range.last - 1 : range.first]
    result = to_a.delete_if do |ele|
      range.to_range.cover? ele
    end

    if cover? range and !(range_mins.include? range.min)
      result << range.min
      result.sort!
    end
    if min_in_but_max_exclude? range
      result << range.min
      result.sort!
    end
    @ranges = result.to_range_list.ranges
  end

  def print
    str = ""
    @ranges.each do |range|
      str += range.minmax
           .to_s.gsub("]", ") ")
           .encode("utf-8")
    end

    str.strip
  end

  def min
    @ranges.map(&:min).min
  end

  def range_mins
    @ranges.map(&:min)
  end

  def max
    @ranges.map(&:max).max - 1
  end

  def range_maxes
    @ranges.map(&:max)
  end

  def exclude? range
    @ranges.empty? ||
      (range.max <= min || range.min > max + 1)
  end

  def cover? range
    range.to_range.to_a.each do |element|
      unless to_a.include? element
        return false
      end
    end

    true
  end

  def to_a
    @ranges.map(&:to_a).flatten.sort.uniq
  end

  def less_than_min? range
    range.min < min
  end

  def lagger_than_max? range
    range.max > max
  end

  def less_than_min_but_max_in? range
    range.min < min and range.max < max
  end

  def min_in_but_max_exclude? range
    range.min >= min and not cover?(range)
  end
end

class Array
  def to_range
    if size == 2
      first..last
    else
      raise NoMethodError
    end
  end

  def to_range_list
    ranges = RangeList.new
    ranges.add([first,first])
    start = first
    self[1..-1].sort.each do |ele|
      if ranges.max == (ele -2)
        ranges.add([start, ele])
      else
        ranges.add([ele, ele])
        start = ele
      end
    end
    ranges
  end
end
